#!/usr/bin/env node
const { Command } = require('commander');
const converter = require('./convert');
const fs = require("fs");

const version = '1.1.0';

const program = new Command();

program
  .name('SwiftJS')
  .description('Framework for making HTML loading times faster')
  .version('1.0.1');

program.command('run')
  .description('Runs the project (needs to be in project\'s working directory)')
  .argument("<port>", "port to serve project")
  .action((s) => {
    fs.readdirSync('chunks').forEach(f => fs.rmSync(`chunks/${f}`, {
      recursive: true,
      force: true
    }));
    converter.run(parseInt(s), version);
  });

program.command('init')
  .description('Initiates a SwiftJS project')
  .argument('<name>', 'name of project')
  .action((str) => {
    fs.mkdirSync(str);
    fs.mkdirSync(`${str}/chunks`);
    fs.mkdirSync(`${str}/render`);
    fs.mkdirSync(`${str}/render/default`);
    fs.writeFileSync(`${str}/render/index.swft`, `---
await Swift.runOnEvent("default/add.js", "add")
Swift.finish();
---
<html>
  <body>
    <h1> Hello! </h1>
    <p> This is an app made with SwiftJS. </p>
    <button $swift.click="add"> Click to add one to the counter below me: </button>
    <p id="counter"> 0 </p>
  </body>
</html>`);
    fs.writeFileSync(`${str}/render/default/add.js`, `// Click-to-add example
document.querySelector('#counter').innerText = parseInt(document.querySelector('#counter').innerText) + 1;`);
    fs.writeFileSync(`${str}/render/worker.js`, `importScripts('https://cdn.jsdelivr.net/npm/comlinkjs@2.3/comlink.global.min.js');

class Fetch {
    constructor() {
        this._baseUrl = "";
        this._defaultHeaders = {};
        this._defaultBody = {};
    }

    getBaseUrl() {
        return this._baseUrl;
    }

    getDefaultHeaders() {
        return this._defaultHeaders;
    }

    getDefaultBody() {
        return this._defaultBody;
    }

    setBaseUrl(baseUrl) {
        this._baseUrl = baseUrl;
    }

    setDefaultHeaders(defaultHeaders) {
        this._defaultHeaders = defaultHeaders;
    }

    setDefaultBody(defaultBody) {
        this._defaultBody = defaultBody;
    }

    async get(endpoint = '') {
        const response = await fetch(this.getBaseUrl() + endpoint)
        const data = await response.arrayBuffer();
        return data;
    }

    async post(endpoint = '', body = undefined, headers = {}) {
        this._send('POST', endpoint, body, headers);
    }

    async put(endpoint = '', body = undefined, headers = {}) {
        this._send('PUT', endpoint, body, headers);
    }

    async delete(endpoint = '', body = undefined, headers = {}) {
        this._send('DELETE', endpoint, body, headers);
    }

    async _send(method, endpoint = '', body = undefined, headers = {}) {
        const response = await fetch(this.getBaseUrl() + endpoint, {
            method,
            headers: { ...this.getDefaultHeaders(), ...headers },
            body: JSON.stringify({ ...this.getDefaultBody(), ...body })
        })
        const data = await response.arrayBuffer();
        return data;
    }
}

Comlink.expose({ Fetch }, self);`);
    
    console.log("Initiated project \""+str+"\"")
  });

program.parse();
