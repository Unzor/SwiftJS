const HTMLParser = require('node-html-parser');
const colors = require('@colors/colors');
const UglifyJS = require("uglify-js");
const zlib = require('zlib');
const c = require('express');
const fs = require("fs");
const x = c();

function rangethrough(sequence, str) {
    var a2 = [];
    str.split(sequence[0]).forEach(function(e) {
        var h = e.split(sequence[1]);
        if (h.length == 1) {
            h = h[0];
        }
        a2.push(h)
    })
    var a3 = [];
    a2.forEach(function(e) {
        var type = typeof e;
        if (type == "object") {
            a3.push(sequence[0] + e[0] + sequence[1]);
            a3.push(e[1]);
        } else {
            a3.push(e);
        }
    })
    return a3;
}

let attrsToArr = (c) => {
    var s = c.split("\"");
    var str = [""];
    var i = 0;

    if (s[s.length - 1].length === 0) s = s.slice(0, -1);
    s.forEach(function(e) {
        if (!e.includes("=")) {
            str[i] += "\"" + e + "\"";
        } else {
            if (!i) {
                str[0] += e;
                str[1] += e;
                i += 1;
            } else {
                str.push(e);
                i += 1;
            }
        }
    })
    str = str.slice(1);
    str[0] = str[0].slice(9);
    str = str.map(x => x.startsWith(" ") ? x.slice(1) : x);
    return str;
}

function swft2html(html) {
    return new Promise((resolve) => {
        var main_code = rangethrough(['---', '---'], html)[1];
        html = rangethrough(['---', '---'], html).slice(2).join('---');

        let transform_html = (c) => {
            var body = HTMLParser.parse(c);
            var allAttrs = [];
            var elems = body.querySelectorAll('*').forEach(x => x.rawAttrs !== '' && x.rawAttrs.includes('$swift.') ? allAttrs.push([x.outerHTML, attrsToArr(x.rawAttrs)]) : null);

            function transform(str) {
                var ev = str.split(".")[1].split("=");
                return "on" + ev[0] + "=" + `"(async () => { await emit('${ev.map(x => x.replaceAll('\""', ''))[1].replaceAll('"', '').replaceAll(" ", "'); await emit('")}')})();"`;
            };

            function arrayFindIncludes(n, r) {
                var u = [];
                return r.forEach(function(r) {
                    r.includes(n) && u.push(r)
                }), u
            }

            allAttrs.forEach(function(e, i) {
                var r = arrayFindIncludes("$swift.", e[1]);
                r.forEach(function(eo, i) {
                    var oe = e[0];
                    e[0] = e[0].replace(eo, transform(eo))
                    c = c.replace(oe, e[0]);
                })
            });
            return c;
        }

        let output = '';

        function rangethrough(sequence, str) {
            var a2 = [];
            str.split(sequence[0]).forEach(function(e) {
                var h = e.split(sequence[1]);
                if (h.length == 1) {
                    h = h[0];
                }
                a2.push(h)
            })
            var a3 = [];
            a2.forEach(function(e) {
                var type = typeof e;
                if (type == "object") {
                    a3.push(sequence[0] + e[0] + sequence[1]);
                    a3.push(e[1]);
                } else {
                    a3.push(e);
                }
            })
            return a3;
        }

        let ord = [];

        if (fs.existsSync('chunks')) {
            fs.rmSync('chunks', {
                recursive: true
            });
            fs.mkdirSync('chunks');
        }

        let curr_chks = {};

        let runOnEvent = (file, _event) => {
            return new Promise((resolve) => {
                let i = 0;
                let ord = [];
                const readStream = fs.createReadStream('render/' + file);
                let d = file.split('/')[file.split('/').length - 1].split('.').slice(0, -1).join('.') + '-' + i;
                readStream.on('data', function(chunk) {
                    d = file.split('/')[file.split('/').length - 1].split('.').slice(0, -1).join('.') + '-' + i++;
                    ord.push([d, zlib.gzipSync(chunk)]);
                });

                readStream.on('end', function() {
                    let nord = [];
                    ord.forEach(function(e) {
                        nord.push('chunks/' + e[0]);
                        fs.writeFileSync('chunks/' + e[0].split('/')[e[0].split('/').length - 1], e[1]);
                        console.log((e[0].split('/')[e[0].split('/').length - 1] + " - " + fs.statSync('chunks/' + e[0].split('/')[e[0].split('/').length - 1]).size / 1000 + "kb").green);
                    });
                    curr_chks[_event] = {
                        chunks: i,
                        name: file.split('/')[file.split('/').length - 1].split('.').slice(0, -1).join('.')
                    };
                    resolve(true);
                });
            });
        }

        let createEvent = (file, _event) => {
            return new Promise((resolve) => {
                let i = 0;
                let ord = [];
                const readStream = fs.createReadStream('render/' + file);
                let d = file.split('/')[file.split('/').length - 1].split('.').slice(0, -1).join('.') + '-' + i;
                readStream.on('data', function(chunk) {
                    d = file.split('/')[file.split('/').length - 1].split('.').slice(0, -1).join('.') + '-' + i++;
                    ord.push([d, zlib.gzipSync(chunk)]);
                });

                readStream.on('end', function() {
                    let nord = [];
                    ord.forEach(function(e) {
                        nord.push('chunks/' + e[0]);
                        fs.writeFileSync('chunks/' + e[0].split('/')[e[0].split('/').length - 1], e[1]);
                        console.log((e[0].split('/')[e[0].split('/').length - 1] + " - " + fs.statSync('chunks/' + e[0].split('/')[e[0].split('/').length - 1]).size / 1000 + "kb").green);
                    });
                    curr_chks[_event] = {
                        chunks: i,
                        name: file.split('/')[file.split('/').length - 1].split('.').slice(0, -1).join('.')
                    };
                    resolve(true);
                });
            });
        }
        let code = {};
        let onEventEnd = (name, func) => {
            code[name] = func.toString();
        }

        function finish() {
            let gcode = UglifyJS.minify(`var onAfterLoad = ${JSON.stringify(code)};
for (var t in onAfterLoad) onAfterLoad[t] = eval(onAfterLoad[t]);
            var it = 0;
var _switch = 0;

window.repeat = function(func, times) {
    it = 0;
    return new Promise(async (resolve) => {
        times && --times && repeat(func, times);
        it++;
        var res = await func(it - 1);
        if (it - 1 == times) {
            resolve(res || 0);
        }
    });
}

var loadedScripts = {};

async function emit(n) {
    var chks = ${JSON.stringify(curr_chks)};
    if (!loadedScripts[n]) {
    async function decompressBlob(blob) {
        let ds = new DecompressionStream("gzip");
        let decompressedStream = blob.stream().pipeThrough(ds);
        return await new Response(decompressedStream).blob();
    }

    function loop(a, f) {
        return new Promise((resolve, reject) => {
            var i = 0;
            let next = () => {
                if (a[i]) {
                    f(a[i], i++, next);
                } else {
                    resolve(true);  
                }
            };
            f(a[i], i++, next);
        });
    }

    window.d = [];
    function jchk(u) {
        return new Promise(r => {
          fetch(u).then(b => r(window.d[parseInt(u.split('-')[1])] = new Blob([b])));
        });
    }

    window.u = [];
    for (var chunk_name in chks) {
        var {
            name,
            chunks
        } = chks[chunk_name];
        if (n == chunk_name) {
            repeat(function(i) {
                jchk('chunks/' + name + '-' + i).then(x => {
                  i++;
                  if (i == chunks) _switch = 1;
                });
            }, chunks);
        }
    };
    let intrv = setInterval(async () => {
    if (_switch) {
    clearInterval(intrv);
    await loop(d, async (b, i, next) => {
        var j = await decompressBlob(b);
        u.push(await j.text());
        next();
    })
    u = [...new Set(u)].join('');
    // d = await d.map(async(x)=>await x.text());
    // d = d.join('');
    loadedScripts[n] = u;
    eval(u);
    console.log(onAfterLoad, n, onAfterLoad[n]);
    if (onAfterLoad[n]) onAfterLoad[n]();
    window.u = [];
    }
    });
    } else {
      Function(loadedScripts[n])();
    }
}`);
            if (gcode.error) throw new Error(gcode.error);
            var cfl = `<script>${UglifyJS.minify(`const Comlink = (function () {
    const TRANSFERABLE_TYPES = [ArrayBuffer, MessagePort];
    const uid = Math.floor(Math.random() * Number.MAX_SAFE_INTEGER);
    const proxyValueSymbol = Symbol("proxyValue");
    const throwSymbol = Symbol("throw");
    const proxyTransferHandler = {
        canHandle: (obj) => obj && obj[proxyValueSymbol],
        serialize: (obj) => {
            const { port1, port2 } = new MessageChannel();
            expose(obj, port1);
            return port2;
        },
        deserialize: (obj) => {
            return proxy(obj);
        }
    };
    const throwTransferHandler = {
        canHandle: (obj) => obj && obj[throwSymbol],
        serialize: (obj) => obj.toString() + "\\n" + obj.stack,
        deserialize: (obj) => {
            throw Error(obj);
        }
    };
    /* export */ const transferHandlers = new Map([
        ["PROXY", proxyTransferHandler],
        ["THROW", throwTransferHandler]
    ]);
    let pingPongMessageCounter = 0;
    /* export */ function proxy(endpoint, target) {
        if (isWindow(endpoint))
            endpoint = windowEndpoint(endpoint);
        if (!isEndpoint(endpoint))
            throw Error("endpoint does not have all of addEventListener, removeEventListener and postMessage defined");
        activateEndpoint(endpoint);
        return cbProxy(async (irequest) => {
            let args = [];
            if (irequest.type === "APPLY" || irequest.type === "CONSTRUCT")
                args = irequest.argumentsList.map(wrapValue);
            const response = await pingPongMessage(endpoint, Object.assign({}, irequest, { argumentsList: args }), transferableProperties(args));
            const result = response.data;
            return unwrapValue(result.value);
        }, [], target);
    }
    /* export */ function proxyValue(obj) {
        obj[proxyValueSymbol] = true;
        return obj;
    }
    /* export */ function expose(rootObj, endpoint) {
        if (isWindow(endpoint))
            endpoint = windowEndpoint(endpoint);
        if (!isEndpoint(endpoint))
            throw Error("endpoint does not have all of addEventListener, removeEventListener and postMessage defined");
        activateEndpoint(endpoint);
        attachMessageHandler(endpoint, async function (event) {
            if (!event.data.id || !event.data.callPath)
                return;
            const irequest = event.data;
            let that = await irequest.callPath
                .slice(0, -1)
                .reduce((obj, propName) => obj[propName], rootObj);
            let obj = await irequest.callPath.reduce((obj, propName) => obj[propName], rootObj);
            let iresult = obj;
            let args = [];
            if (irequest.type === "APPLY" || irequest.type === "CONSTRUCT")
                args = irequest.argumentsList.map(unwrapValue);
            if (irequest.type === "APPLY") {
                try {
                    iresult = await obj.apply(that, args);
                }
                catch (e) {
                    iresult = e;
                    iresult[throwSymbol] = true;
                }
            }
            if (irequest.type === "CONSTRUCT") {
                try {
                    iresult = new obj(...args); // eslint-disable-line new-cap
                    iresult = proxyValue(iresult);
                }
                catch (e) {
                    iresult = e;
                    iresult[throwSymbol] = true;
                }
            }
            if (irequest.type === "SET") {
                obj[irequest.property] = irequest.value;
                iresult = true;
            }
            iresult = makeInvocationResult(iresult);
            iresult.id = irequest.id;
            return endpoint.postMessage(iresult, transferableProperties([iresult]));
        });
    }
    function wrapValue(arg) {
        // Is arg itself handled by a TransferHandler?
        for (const [key, transferHandler] of transferHandlers) {
            if (transferHandler.canHandle(arg)) {
                return {
                    type: key,
                    value: transferHandler.serialize(arg)
                };
            }
        }
        // If not, traverse the entire object and find handled values.
        let wrappedChildren = [];
        for (const item of iterateAllProperties(arg)) {
            for (const [key, transferHandler] of transferHandlers) {
                if (transferHandler.canHandle(item.value)) {
                    wrappedChildren.push({
                        path: item.path,
                        wrappedValue: {
                            type: key,
                            value: transferHandler.serialize(item.value)
                        }
                    });
                }
            }
        }
        for (const wrappedChild of wrappedChildren) {
            const container = wrappedChild.path
                .slice(0, -1)
                .reduce((obj, key) => obj[key], arg);
            container[wrappedChild.path[wrappedChild.path.length - 1]] = null;
        }
        return {
            type: "RAW",
            value: arg,
            wrappedChildren
        };
    }
    function unwrapValue(arg) {
        if (transferHandlers.has(arg.type)) {
            const transferHandler = transferHandlers.get(arg.type);
            return transferHandler.deserialize(arg.value);
        }
        else if (isRawWrappedValue(arg)) {
            for (const wrappedChildValue of arg.wrappedChildren || []) {
                if (!transferHandlers.has(wrappedChildValue.wrappedValue.type))
                    throw Error(\`Unknown value type "\${arg.type}" at \${wrappedChildValue.path.join(".")}\`);
                const transferHandler = transferHandlers.get(wrappedChildValue.wrappedValue.type);
                const newValue = transferHandler.deserialize(wrappedChildValue.wrappedValue.value);
                replaceValueInObjectAtPath(arg.value, wrappedChildValue.path, newValue);
            }
            return arg.value;
        }
        else {
            throw Error(\`Unknown value type "\${arg.type}"\`);
        }
    }
    function replaceValueInObjectAtPath(obj, path, newVal) {
        const lastKey = path.slice(-1)[0];
        const lastObj = path
            .slice(0, -1)
            .reduce((obj, key) => obj[key], obj);
        lastObj[lastKey] = newVal;
    }
    function isRawWrappedValue(arg) {
        return arg.type === "RAW";
    }
    function windowEndpoint(w) {
        if (self.constructor.name !== "Window")
            throw Error("self is not a window");
        return {
            addEventListener: self.addEventListener.bind(self),
            removeEventListener: self.removeEventListener.bind(self),
            postMessage: (msg, transfer) => w.postMessage(msg, "*", transfer)
        };
    }
    function isEndpoint(endpoint) {
        return ("addEventListener" in endpoint &&
            "removeEventListener" in endpoint &&
            "postMessage" in endpoint);
    }
    function activateEndpoint(endpoint) {
        if (isMessagePort(endpoint))
            endpoint.start();
    }
    function attachMessageHandler(endpoint, f) {
        endpoint.addEventListener("message", f);
    }
    function detachMessageHandler(endpoint, f) {
        // Same as above.
        endpoint.removeEventListener("message", f);
    }
    function isMessagePort(endpoint) {
        return endpoint.constructor.name === "MessagePort";
    }
    function isWindow(endpoint) {
        // TODO: This doesn’t work on cross-origin iframes.
        // return endpoint.constructor.name === 'Window';
        return ["window", "length", "location", "parent", "opener"].every(prop => prop in endpoint);
    }

    function pingPongMessage(endpoint, msg, transferables) {
        const id = \`\${uid}-\${pingPongMessageCounter++}\`;
        return new Promise(resolve => {
            attachMessageHandler(endpoint, function handler(event) {
                if (event.data.id !== id)
                    return;
                detachMessageHandler(endpoint, handler);
                resolve(event);
            });
            msg = Object.assign({}, msg, { id });
            endpoint.postMessage(msg, transferables);
        });
    }
    function cbProxy(cb, callPath = [], target = function () { }) {
        return new Proxy(target, {
            construct(_target, argumentsList, proxy) {
                return cb({
                    type: "CONSTRUCT",
                    callPath,
                    argumentsList
                });
            },
            apply(_target, _thisArg, argumentsList) {
                if (callPath[callPath.length - 1] === "bind")
                    return cbProxy(cb, callPath.slice(0, -1));
                return cb({
                    type: "APPLY",
                    callPath,
                    argumentsList
                });
            },
            get(_target, property, proxy) {
                if (property === "then" && callPath.length === 0) {
                    return { then: () => proxy };
                }
                else if (property === "then") {
                    const r = cb({
                        type: "GET",
                        callPath
                    });
                    return Promise.resolve(r).then.bind(r);
                }
                else {
                    return cbProxy(cb, callPath.concat(property), _target[property]);
                }
            },
            set(_target, property, value, _proxy) {
                return cb({
                    type: "SET",
                    callPath,
                    property,
                    value
                });
            }
        });
    }
    function isTransferable(thing) {
        return TRANSFERABLE_TYPES.some(type => thing instanceof type);
    }
    function* iterateAllProperties(value, path = [], visited = null) {
        if (!value)
            return;
        if (!visited)
            visited = new WeakSet();
        if (visited.has(value))
            return;
        if (typeof value === "string")
            return;
        if (typeof value === "object")
            visited.add(value);
        if (ArrayBuffer.isView(value))
            return;
        yield { value, path };
        const keys = Object.keys(value);
        for (const key of keys)
            yield* iterateAllProperties(value[key], [...path, key], visited);
    }
    function transferableProperties(obj) {
        const r = [];
        for (const prop of iterateAllProperties(obj)) {
            if (isTransferable(prop.value))
                r.push(prop.value);
        }
        return r;
    }
    function makeInvocationResult(obj) {
        for (const [type, transferHandler] of transferHandlers) {
            if (transferHandler.canHandle(obj)) {
                const value = transferHandler.serialize(obj);
                return {
                    value: { type, value }
                };
            }
        }
        return {
            value: {
                type: "RAW",
                value: obj
            }
        };
    }
    return { proxy, proxyValue, transferHandlers, expose };
})();
const twrk = new Worker("./worker.js");
const nwrk=Comlink.proxy(twrk);
window.fetch = (async e => { return(await new nwrk.Fetch).get(e) });
${gcode.code}`).code}
</script>`;
            var t_html = HTMLParser.parse(transform_html(html));
            var scr = HTMLParser.parse(cfl);
            t_html.querySelector('html').appendChild(scr);
            resolve(t_html.outerHTML);
        }
        var Swift = {
            runOnEvent,
            onEventEnd,
            finish
        };
        eval('(async () => { \n' + main_code + '\n})()');
    });
}


function run(p, v) {
    var entries = fs.readdirSync('render');
    entries.forEach(async function(entry) {
        if (fs.statSync('render/' + entry).isFile()) {
            var h = fs.readFileSync('render/' + entry).toString();
            fs.writeFileSync('render/' + entry.split('.').slice(0, -1).join('.') + '.html', await swft2html(h));
        }
    });
    console.log(("SwiftJS " + v).cyan);
    x.use('/', c.static('render'));
    x.use('/chunks', c.static('chunks'))
    x.listen(p || 8080, () => console.log(`Server up at port ${p||8080} (http://localhost:${p||8080})`.red));
};

module.exports = {
    run,
    swft2html
};
