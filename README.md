![SwiftJS](images/logo.png)

A JavaScript framework to help make website page loading times faster.
# Installation
```
npm i -g @diegab/swift
```
# Usage
First, initiate a project using this command:
```
swft init my-first-project
```
A few scripts are preadded when initiated.
Change into your project and run the project:
```
cd my-first-project
swft run 8080
```
Finally, go to [localhost:8080](http://localhost:8080) and visit your page in action.

You should see a popup saying "Hello!". Then open "Inspect Element", go to "Network" and reload the page.

You should see that a resource is loaded, but without a .js extension. When you open it in "Preview", you will see it is not JavaScript code.

That is because SwiftJS will split the file into chunks and compress each one using GZip. The browser will then decompress it, join the chunks together and run the result.

# Using SwiftJS
SwiftJS uses the .swft file extension instead of normal HTML, although it translates to HTML.
When you open it, you will see this:
```html
---
await Swift.runOnEvent("default/hello.js", "hello");
Swift.finish();
---
<html>
  <body $swift.load="hello">
    <h1> Hello! </h1>
  </body>
</html>
```
This is HTML and JavaScript code mixed into one file. The top (separated by three lines) is the code that handles the events. 

The `Swift.runOnEvent` function runs a file when an event is triggered. To trigger those events, you need to add an attribute named "$swift.{event here}" in any element (replace event here with any event). 

Notice the `Swift.finish()` function? It is the function that translates and serves the normal HTML code.

# License
```
Apache License
Version 2.0, January 2004
http://www.apache.org/licenses/
```
